# Customized Monitoring checks and Eventhandler

## Overview
This Repository contains custom checks and handlers that will be deployed via Ansible for resolving basic problems

## Checks

### check_tomcat.py
This script uses the Tomcat manager XML-webinterface to collect basic data and parses them into a readable format for Icinga2 including performance data.

### collect_apt_packages.py
This script is an enhanced version of the default apt check that Icinga2 provides.
it collects data directly from the apt api and build a complete list of:
- available updates
- current version
- new version


## Events

### restart_service
A small python script that tries to restart a service once Icinga detects it not running.
