#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import json
try:
    from urllib.error import HTTPError
except ImportError:
    from urllib2 import HTTPError

import requests
import sys

from enum import Enum
import logging
from requests import RequestException, ConnectionError, URLRequired, TooManyRedirects, Timeout


class Check_phpFPM(object):
    """docstring for Check_phpFPM."""

    class Exitcode(Enum):
        """
        Enum Class to better select Exitcodes
        """
        OK = 0
        WARNING = 1
        CRITICAL = 2
        UNKNOWN = 3

    def __init__(self, arg):
        self.arg = arg

        self.log = logging.getLogger('check_phpFPM')
        streamhandler = logging.StreamHandler(sys.stdout)
        self.log.addHandler(streamhandler)
        self.log.setLevel(logging.INFO)

    def get_stats(self):

        try:
            req = requests.get(self.arg.url + "?json&full", auth=(self.arg.username, self.arg.password), verify=False)
            req.raise_for_status()
            self.data = json.loads(req.text)

        except (RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout) as ex:
            self.log.error("PHP-FPM - CRITICAL {}".format(ex.message))
            sys.exit(self.Exitcode.CRITICAL.value)

    def perf_data(self):
        """
        :rtype: object
        """

        perf_array = []

        proc_warn = ""
        proc_crit = ""

        statuscode = self.Exitcode.OK.value

        perf_array.append(" {}={};{};{};{};{}".format("conn_count", self.data['accepted conn'], "", "", "", ""))
        perf_array.append(" {}={};{};{};{};{}".format("active_proc", self.data['active processes'], "", "", "", self.data[
            'max active processes']))
        perf_array.append(" {}={};{};{};{};{}".format("listen_queue", self.data['listen queue'], "", "", "", self.data[
            'max listen queue']))

        proc_count = 0

        for element in self.data["processes"]:
            proc_count += 1

        if self.arg.warning:
            proc_warn = self.arg.warning
            statuscode = self.compare_perf_data(proc_count, proc_warn, self.Exitcode.WARNING)

        if self.arg.critical:
            proc_crit = self.arg.critical
            statuscode = self.compare_perf_data(proc_count, proc_crit, self.Exitcode.CRITICAL)

        perf_array.append(" {}={};{};{};{};{}".format("proc_count", proc_count, proc_warn, proc_crit, "", ""))

        return perf_array, statuscode

    def compare_perf_data(self, arg1, arg2, Exitcode):
        """
        Conmpares arg1 and arg2, returns Exitcode when arg1 is bigger than arg2.
        Returns Exitcode Enum OK if not
        :param arg1:
        :param arg2:
        :param Exitcode:
        :return:
        """

        if arg1 > arg2:
            return Exitcode
        else:
            return self.Exitcode.OK

    def info_data(self):
        """
        :rtype: object
        """
        data_array = []

        data_array.append("Active Processes: %s \n" % self.data['active processes'])
        data_array.append("Queue length: %s \n" % self.data['listen queue len'])
        data_array.append("Process manager: %s \n" % self.data['process manager'])

        return data_array

    def print_status(self):
        """
        :rtype: object
        """
        perf_data, statuscode = self.perf_data()
        Exitcode = self.Exitcode(statuscode)

        data = self.info_data()
        return_String = "PHP-FPM - {} ".format(Exitcode.name)

        for element in data:
            return_String += element

        return_String += "|"

        for perf_element in perf_data:
            return_String += perf_element

        self.log.info(return_String)
        sys.exit(Exitcode.value)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument('--host', default='http://wiki.mw-krz-swd.de/status', help='URL to the NginX statuspage')
    parser.add_argument('-u', '--username', help='Username to be used for login')
    parser.add_argument('-p', '--password', help='Password to be used for login')
    parser.add_argument('-c', '--critical', help='Critical process count')
    parser.add_argument('-w', '--warning', help='Warning process count')
    args = parser.parse_args()
    c = Check_phpFPM(args)
    c.get_stats()
    c.print_status()
