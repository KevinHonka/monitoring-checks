#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import logging
import re

import requests
import sys

from enum import Enum
from lxml import etree
from lxml import objectify
from requests import RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout


class Check_Tomcat(object):
    """docstring for Check_Tomcat."""

    _perfdata = []
    tr = None

    class ExitCode(Enum):
        """
        Enum Class to better select ExitCodes
        """
        OK = 0
        WARNING = 1
        CRITICAL = 2
        UNKNOWN = 3

    def __init__(self, arg):
        """
        making args available for further use.
        Also setting a log handler as to not use the pring function.
        :param arg:
        """
        self.arg = arg

        self.log = logging.getLogger('Check_Tomcat')
        streamhandler = logging.StreamHandler(sys.stdout)
        self.log.addHandler(streamhandler)
        self.log.setLevel(logging.INFO)

    def get_tomcat_stats(self):
        """
        Getting the Data from the XML Interface of the Tomcat server
        setting class variable tr which contains a parsed xml document for further access
        """

        parser = etree.XMLParser(encoding="utf-8")

        try:
            req = requests.get(self.arg.host, auth=(self.arg.username, self.arg.password))
            req.raise_for_status()
            xml = req.text

            xml = re.sub(r'\bencoding="[-\w]+"', '', xml, count=1)

            self.tr = objectify.fromstring(xml, parser)

        except (RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout) as ex:
            self.log.error("TOMCAT - CRITICAL {}".format( ex.message))
            sys.exit(self.ExitCode.CRITICAL.value)

    def print_status(self):
        """
        Using the stored data to fill a multiline string which is then printed via a log handler
        """
        return_String = "TOMCAT - ok |"
        _perfdata = ""

        for appt in self.tr.getchildren():
            for e in appt.getchildren():
                if e.tag == "memory":
                    _perfdata += " {}={};{};{};{};{}".format(' ' + e.tag, e.attrib["total"], "", "", "",
                                                             e.attrib["max"])
                elif e.tag == "memorypool":
                    _perfdata += " {}={};{};{};{};{}".format(' ' + e.attrib["name"] + " " + e.attrib["type"],
                                                             e.attrib["usageUsed"], "", "", "", e.attrib["usageMax"])
                elif e.tag == "threadInfo":
                    _perfdata += " {}={};{};{};{};{}".format(' ' + e.tag, e.attrib["currentThreadCount"], "", "", "",
                                                             e.attrib["maxThreads"])
                    _perfdata += " {}={};{};{};{};{}".format(' Busy Threads', e.attrib["currentThreadsBusy"], "", "",
                                                             "", "")
                elif e.tag == "requestInfo":
                    _perfdata += " {}={};{};{};{};{}".format(' requestCount', e.attrib["requestCount"], "", "", "", "")
                    _perfdata += " {}={};{};{};{};{}".format(' Errors', e.attrib["errorCount"], "", "", "", "")
                elif e.tag == "workers":
                    count = 0
                    for worker in e.getchildren():
                        if worker.attrib["remoteAddr"] != "?":
                            count += 1
                    _perfdata += " {}={};{};{};{};{}".format(' Active workers', count, "", "", "", "")

        return_String += _perfdata

        self.log.info(return_String)
        sys.exit(self.ExitCode.OK.value)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
       formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument('--host', default='http://127.0.0.1:8080/manager/status/all?XML=true',
                    help='URL to the Tomcat manager')
    parser.add_argument('-u', '--username', default='admin', help='Username to be used to login')
    parser.add_argument('-p', '--password', default='passw0rd', help='Password to be used to login')
    args = parser.parse_args()
    c = Check_Tomcat(args)
    c.get_tomcat_stats()
    c.print_status()
