#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import json
import logging
import sys
from collections import namedtuple

import requests
from requests import RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout


class CheckAWX3(object):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

    ping = "/api/v2/ping/"
    instances = "/api/v2/instances/"
    instance_groups = "/api/v2/instance_groups/"
    config = "/api/v2/config/"
    settings = "/api/v2/settings/"
    me = "/api/v2/me/"
    dashboard = "/api/v2/dashboard/"
    organizations = "/api/v2/organizations/"
    users = "/api/v2/users/"
    projects = "/api/v2/projects/"
    project_updates = "/api/v2/project_updates/"
    teams = "/api/v2/teams/"
    credentials = "/api/v2/credentials/"
    credential_types = "/api/v2/credential_types/"
    applications = "/api/v2/applications/"
    tokens = "/api/v2/tokens/"
    inventory = "/api/v2/inventories/"
    inventory_scripts = "/api/v2/inventory_scripts/"
    inventory_sources = "/api/v2/inventory_sources/"
    inventory_updates = "/api/v2/inventory_updates/"
    groups = "/api/v2/groups/"
    hosts = "/api/v2/hosts/"
    job_templates = "/api/v2/job_templates/"
    jobs = "/api/v2/jobs/"
    job_events = "/api/v2/job_events/"
    ad_hoc_commands = "/api/v2/ad_hoc_commands/"
    system_job_templates = "/api/v2/system_job_templates/"
    system_jobs = "/api/v2/system_jobs/"
    schedules = "/api/v2/schedules/"
    roles = "/api/v2/roles/"
    notification_templates = "/api/v2/notification_templates/"
    notifications = "/api/v2/notifications/"
    labels = "/api/v2/labels/"
    unified_job_templates = "/api/v2/unified_job_templates/"
    unified_jobs = "/api/v2/unified_jobs/"
    activity_stream = "/api/v2/activity_stream/"
    workflow_job_templates = "/api/v2/workflow_job_templates/"
    workflow_jobs = "/api/v2/workflow_jobs/"
    workflow_job_template_nodes = "/api/v2/workflow_job_template_nodes/"
    workflow_job_nodes = "/api/v2/workflow_job_nodes/"

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, url):
        self.__url = url

    @property
    def username(self):
        return self.__username

    @username.setter
    def username(self, username):
        if username is not None:
            self.__username = username
        else:
            raise ValueError()

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, password):
        if password is not None:
            self.__password = password
        else:
            raise ValueError()

    @property
    def code(self):
        return self.__code

    @code.setter
    def code(self, code):
        self.__code = code

    def __init__(self, args, url="", username="", password=""):

        self.log = logging.getLogger("Log1")
        streamhandler = logging.StreamHandler(sys.stdout)
        self.log.addHandler(streamhandler)
        self.log.setLevel(logging.INFO)

        self.url = url
        self.username = args.username
        self.password = args.password
        self.args = args

    def get_data(self, parameter):
        try:
            req = requests.get(self.url + parameter, auth=(self.username, self.password))
            req.raise_for_status()
            return json.loads(req.text, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

        except (RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout) as ex:
            self.log.error(ex.message)

    def get_health_status(self, ha=None, version=None):

        health_data = self.get_data()
        status_list = []
        if ha:
            if ha != health_data.ha:
                status_list.append(self.code(status_code=self.CRITICAL,
                                             message='HA Status is not as expected. Value:{}'.format(
                                                 health_data.ha)))
            else:
                status_list.append(self.code(status_code=self.OK,
                                             message='HA Status is as expected. Value:{}'.format(
                                                 health_data.ha)))
        exit_code = 0
        exit_message = ""

        for status in status_list:

            if status.code.value > exit_code:
                exit_code = status.code.value

            exit_message += status.message + '\n'

        if exit_message == "":
            exit_message = "OK | Everything seems to run fine"

        self.log.info(exit_message)
        sys.exit(exit_code)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter
    )

    cli = argparse.ArgumentParser(
        # creates parser

        formatter_class=argparse.RawTextHelpFormatter,
        epilog="Thanks for using me"
    )

    subparsers = cli.add_subparsers(dest="subcommand")

    health_parser = subparsers.add_parser('health', help="blub")

    cli.add_argument('-u', '--username', required=False, default="admin", help='Username for AWX')

    cli.add_argument('-p', '--password', required=False, default="password", help='password for AWX')

    cli.add_argument('--host', required=False, default="http://127.0.0.1", help='URL for AWX')

    cli.add_argument('--apiversion', required=False, default=2, help='API Version for AWX')

    subparsers = parser.add_subparsers(dest='command')

    subparsers.add_parser('health')

    args = parser.parse_args()
    check = CheckAWX3()
    if args.command == 'health':
        check.get_health_status()
    else:
        print("invalid call! not enough parameters")
        sys.exit(1)
