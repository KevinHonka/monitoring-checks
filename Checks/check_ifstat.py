#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import re
import argparse

class Check_IFStat(object):
    """docstring for Check_IFStat."""

    def __init__(self):
        super(Check_IFStat, self).__init__()

    def get_io_stats(self):

        try:
            with open('/proc/net/dev', 'r') as f:
                content = f.readlines()
            content = [x.strip() for x in content]

            content.pop(2)
            content.pop(1)
            content.pop(0)

            self.content = content

        except Exception as e:
            raise SystemExit("Interface - CRITICAL Could_not fetch interface data")


    def print_status(self):
        return_String = "Interface - ok |"
        _perfdata = ""
        template = re.compile("(?P<interface>[\d\w]*):\s+(?P<rBytes>\d*)\s+(?P<rPackets>\d*)\s+(?P<rErrors>\d*)\s+(?P<rDrop>\d*)\s+(?P<rFIFO>\d*)\s+(?P<rFRAME>\d*)\s+(?P<rCOMPRESSED>\d*)\s+(?P<rMULTICAST>\d*)\s+(?P<tBytes>\d*)\s+(?P<tPackets>\d*)\s+(?P<tErrors>\d*)\s+(?P<tDrop>\d*)\s+(?P<tFIFO>\d*)\s+(?P<tFRAME>\d*)\s+(?P<tCOMPRESSED>\d*)\s+(?P<tMULTICAST>\d*)")

        for line in self.content:
            match = template.search(line)
            if match:
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_received_BYTES", match.group("rBytes"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_received_PACKETS", match.group("rPackets"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_received_ERRORS", match.group("rErrors"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_received_DROP", match.group("rDrop"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_received_FIFO", match.group("rFIFO"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_received_FRAMES", match.group("rFRAME"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_received_COMPRESSED", match.group("rCOMPRESSED"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_received_MULTICAST", match.group("rMULTICAST"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_transmitted_BYTES", match.group("tBytes"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_transmitted_PACKETS", match.group("tPackets"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_transmitted_ERRORS", match.group("tErrors"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_transmitted_DROP", match.group("tDrop"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_transmitted_FIFO", match.group("tFIFO"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_transmitted_FRAMES", match.group("tFRAME"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_transmitted_COMPRESSED", match.group("tCOMPRESSED"), "", "", "", "")
                _perfdata += " {}={};{};{};{};{}".format(match.group("interface") + "_transmitted_MULTICAST", match.group("tMULTICAST"), "", "", "", "")
        return_String += _perfdata

        print(return_String)


if __name__ == '__main__':
    c = Check_IFStat()
    c.get_io_stats()
    c.print_status()
