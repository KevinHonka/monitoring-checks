#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import json
import logging
import sys
from collections import namedtuple
from pprint import pformat

import requests
from enum import Enum
from requests import RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout


class CheckAWX(object):
    """docstring for CheckAWX."""

    class API:
        class URIv1(Enum):
            """Enum class for URIs"""
            ping = "/api/v1/ping/"
            instances = "/api/v1/instances/"
            instance_groups = "/api/v1/instance_groups/"
            config = "/api/v1/config/"
            settings = "/api/v1/settings/"
            me = "/api/v1/me/"
            dashboard = "/api/v1/dashboard/"
            organizations = "/api/v1/organizations/"
            users = "/api/v1/users/"
            projects = "/api/v1/projects/"
            project_updates = "/api/v1/project_updates/"
            teams = "/api/v1/teams/"
            credentials = "/api/v1/credentials/"
            inventory = "/api/v1/inventories/"
            inventory_scripts = "/api/v1/inventory_scripts/"
            inventory_sources = "/api/v1/inventory_sources/"
            inventory_updates = "/api/v1/inventory_updates/"
            groups = "/api/v1/groups/"
            hosts = "/api/v1/hosts/"
            job_templates = "/api/v1/job_templates/"
            jobs = "/api/v1/jobs/"
            job_events = "/api/v1/job_events/"
            ad_hoc_commands = "/api/v1/ad_hoc_commands/"
            system_job_templates = "/api/v1/system_job_templates/"
            system_jobs = "/api/v1/system_jobs/"
            schedules = "/api/v1/schedules/"
            roles = "/api/v1/roles/"
            notification_templates = "/api/v1/notification_templates/"
            notifications = "/api/v1/notifications/"
            labels = "/api/v1/labels/"
            unified_job_templates = "/api/v1/unified_job_templates/"
            unified_jobs = "/api/v1/unified_jobs/"
            activity_stream = "/api/v1/activity_stream/"
            workflow_job_templates = "/api/v1/workflow_job_templates/"
            workflow_jobs = "/api/v1/workflow_jobs/"
            workflow_job_template_nodes = "/api/v1/workflow_job_template_nodes/"
            workflow_job_nodes = "/api/v1/workflow_job_nodes/"

        class URIv2(Enum):
            """Enum class for URIs"""
            ping = "/api/v2/ping/"
            instances = "/api/v2/instances/"
            instance_groups = "/api/v2/instance_groups/"
            config = "/api/v2/config/"
            settings = "/api/v2/settings/"
            me = "/api/v2/me/"
            dashboard = "/api/v2/dashboard/"
            organizations = "/api/v2/organizations/"
            users = "/api/v2/users/"
            projects = "/api/v2/projects/"
            project_updates = "/api/v2/project_updates/"
            teams = "/api/v2/teams/"
            credentials = "/api/v2/credentials/"
            credential_types = "/api/v2/credential_types/"
            applications = "/api/v2/applications/"
            tokens = "/api/v2/tokens/"
            inventory = "/api/v2/inventories/"
            inventory_scripts = "/api/v2/inventory_scripts/"
            inventory_sources = "/api/v2/inventory_sources/"
            inventory_updates = "/api/v2/inventory_updates/"
            groups = "/api/v2/groups/"
            hosts = "/api/v2/hosts/"
            job_templates = "/api/v2/job_templates/"
            jobs = "/api/v2/jobs/"
            job_events = "/api/v2/job_events/"
            ad_hoc_commands = "/api/v2/ad_hoc_commands/"
            system_job_templates = "/api/v2/system_job_templates/"
            system_jobs = "/api/v2/system_jobs/"
            schedules = "/api/v2/schedules/"
            roles = "/api/v2/roles/"
            notification_templates = "/api/v2/notification_templates/"
            notifications = "/api/v2/notifications/"
            labels = "/api/v2/labels/"
            unified_job_templates = "/api/v2/unified_job_templates/"
            unified_jobs = "/api/v2/unified_jobs/"
            activity_stream = "/api/v2/activity_stream/"
            workflow_job_templates = "/api/v2/workflow_job_templates/"
            workflow_jobs = "/api/v2/workflow_jobs/"
            workflow_job_template_nodes = "/api/v2/workflow_job_template_nodes/"
            workflow_job_nodes = "/api/v2/workflow_job_nodes/"

        @property
        def uri_list(self):
            return self.__uri_list

        @uri_list.setter
        def uri_list(self, api_version):
            if api_version == 1:
                self.__uri_list = self.URIv1
            elif api_version == 2:
                self.__uri_list = self.URIv2
            else:
                self.log.error("Unknown version")

            print("set urilist to {}".format(pformat(self.__uri_list)))

        @property
        def version(self):
            return self.__version

        @version.setter
        def version(self, api_version):
            self.__version = api_version

        @property
        def url(self):
            return self.__url

        @url.setter
        def url(self, url):
            self.__url = url

        def __init__(self, api_version=2, url=""):

            self.log = logging.getLogger('API')
            streamhandler = logging.StreamHandler(sys.stdout)
            self.log.addHandler(streamhandler)
            self.log.setLevel(logging.INFO)

            self.url = url
            self.version = api_version
            if self.version == 1:
                self.__uri_list = self.URIv1
            elif self.version == 2:
                self.__uri_list = self.URIv2
            else:
                self.log.error("Unknown version")

        def get_data(self, uri, username, password, parameter=""):
            """
            Getting the Data from AWX API
            setting class variable tr which contains a parsed xml document for further access

            :returns object parsed from json
            """

            try:
                req = requests.get(self.url + uri + parameter, auth=(username, password))
                req.raise_for_status()

                return json.loads(req.text, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

            except (RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout) as ex:
                self.log.error(ex.message)

    class User:

        def __init__(self, username="", password=""):
            self.username = username
            self.password = password

        @property
        def username(self):
            return self.__username

        @username.setter
        def username(self, username):
            if username is not None:
                self.__username = username
            else:
                raise ValueError()

        @property
        def password(self):
            return self.__password

        @password.setter
        def password(self, password):
            if password is not None:
                self.__password = password
            else:
                raise ValueError()

    class Health:

        class Status:

            class ExitCode(Enum):
                """
                Enum Class to better select ExitCodes
                """
                OK = 0
                WARNING = 1
                CRITICAL = 2
                UNKNOWN = 3

            def __init__(self, status_code=None, message=None):
                self.code = status_code
                self.message = message

            @property
            def code(self):
                return self.__code

            @code.setter
            def code(self, code):
                self.__code = code

            @property
            def message(self):
                return self.__message

            @message.setter
            def message(self, message):
                self.__message = message

        def __init__(self, user=None, api=None):
            self.log = logging.getLogger('Health')
            streamhandler = logging.StreamHandler(sys.stdout)
            self.log.addHandler(streamhandler)
            self.log.setLevel(logging.INFO)

            self.user = user
            self.api = api

        def run(self, args=None):
            health_status = self.get_health(ha=args.ha, version=args.version)
            self.parse_status(health_status)

        def get_health(self, ha=None, version=None):
            health_data = self.api.get_data(uri=self.api.uri_list.ping.value,
                                            username=self.user.username,
                                            password=self.user.password)
            status_list = []

            if ha:
                if ha != health_data.ha:
                    status_list.append(self.Status(status_code=self.Status.ExitCode.CRITICAL,
                                                   message='HA Status is not as expected. Value:{}'.format(
                                                       health_data.ha)))
                else:
                    status_list.append(self.Status(status_code=self.Status.ExitCode.OK,
                                                   message='HA Status is as expected. Value:{}'.format(
                                                       health_data.ha)))

            if version:
                if version != health_data.version:
                    status_list.append(self.Status(status_code=self.Status.ExitCode.CRITICAL,
                                                   message='Version is not as expected. Value:{}'.format(
                                                       health_data.version)))
                else:
                    status_list.append(self.Status(status_code=self.Status.ExitCode.OK,
                                                   message='Version is as expected. Value:{}'.format(
                                                       health_data.version)))

            return status_list

        def parse_status(self, health_status):

            exit_code = 0

            exit_message = ""

            for status in health_status:

                if status.code.value > exit_code:
                    exit_code = status.code.value

                exit_message += status.message + '\n'

            if exit_message == "":
                exit_message = "OK | Everything seems to run fine"

            self.log.info(exit_message)
            sys.exit(exit_code)

    api = API()
    user = User()
    health = Health()

    def __init__(self, args):
        self.log = logging.getLogger('CheckAWX')
        streamhandler = logging.StreamHandler(sys.stdout)
        self.log.addHandler(streamhandler)
        self.log.setLevel(logging.INFO)

        self.args = args

        self.api = self.API(api_version=args.apiversion, url=args.url)
        self.user = self.User(username=args.username, password=args.password)

        self.health = self.Health(user=self.user, api=self.api)
        self.available_commands = {'health': self.health.run}

    def run(self):
        """
        running subcommand based tasks
        :return:
        """

        self.available_commands.get(self.args.subcommand, lambda: 'Invalid')(self.args)

        pass


cli = argparse.ArgumentParser(
    formatter_class=argparse.RawTextHelpFormatter,
    epilog="Thanks for using me"
)

subparsers = cli.add_subparsers(dest="subcommand")


def add_default_args():
    cli.add_argument('-u', '--username', required=False, default="admin", help='Username for AWX')
    cli.add_argument('-p', '--password', required=False, default="password", help='password for AWX')
    cli.add_argument('--host', required=False, default="http://127.0.0.1", help='URL for AWX')
    cli.add_argument('--apiversion', required=False, default=2, help='API Version for AWX')


def add_health_parser():
    health_parser = subparsers.add_parser('health', help="blub")

    health_parser.add_argument('--ha', default=False, required=False, action='store_true',
                               help="Check if HA is enabled")
    health_parser.add_argument('--version', default=None, required=False, help="Check if version is matching")


def add_subcommands():
    add_health_parser()


if __name__ == '__main__':
    add_default_args()
    add_subcommands()
    args = cli.parse_args()
    check = CheckAWX(args)
    check.run()
