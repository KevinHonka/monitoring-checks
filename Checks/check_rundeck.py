#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import json
import logging
import sys
from enum import Enum

import requests
from requests import RequestException, ConnectionError, URLRequired, TooManyRedirects, Timeout


class CheckRundeck:
    """docstring for Check_Rundeck."""

    class ExitCode(Enum):
        """
        Enum Class to better select ExitCodes
        """
        OK = 0
        WARNING = 1
        CRITICAL = 2
        UNKNOWN = 3

    @property
    def token(self):
        return self.__token

    @token.setter
    def token(self, token):
        if token is not None:
            self.__token = token
        else:
            raise ValueError()

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, url):
        if url is not None:
            self.__url = url
        else:
            raise ValueError()

    def __init__(self, ):
        self.__url = None
        self.__token = None

        self.log = logging.getLogger('CheckRundeck')
        streamhandler = logging.StreamHandler(sys.stdout)
        self.log.addHandler(streamhandler)
        self.log.setLevel(logging.INFO)

    def get_health_status(self):

        health_data = self.get_data(self.url + '/api/25/metrics/healthcheck',
                                    headers={'X-Rundeck-Auth-Token': self.token})
        system_data = self.get_data(self.url + '/api/14/system/info',
                             headers={'X-Rundeck-Auth-Token': self.token, 'Accept': 'application/json'})

        return_data = {
            'Threadpool Health': 'OK' if health_data['quartz.scheduler.threadPool']['healthy'] else 'BAD',
            'Datasource Health': 'OK' if health_data['dataSource.connection.time']['healthy'] else 'BAD',
            'Datasource Message': health_data['dataSource.connection.time']['message'],
            'Version': system_data['system']['rundeck']['version'],
            'Up since': system_data['system']['stats']['uptime']['since']['datetime'],
        }

        return_string = self.build_string(return_data)

        self.log.info(return_string)

        if return_data['Datasource Health'] == 'OK':
            sys.exit(self.ExitCode.OK.value)
        elif return_data['Datasource Health'] == 'BAD':
            sys.exit(self.ExitCode.CRITICAL.value)
        else:
            sys.exit(self.ExitCode.UNKNOWN.value)

    def build_string(self, string_values, string_begin="Rundeck - OK \n"):
        return_string = string_begin

        for key, value in string_values.items():
            return_string += " {}: {} \n".format(key, value)

        return return_string

    def get_data(self, url, headers):
        try:
            req = requests.get(url, headers=headers)
            req.raise_for_status()
            data = json.loads(req.text)

            return data

        except (RequestException, ConnectionError, URLRequired, TooManyRedirects, Timeout) as ex:
            self.log.error("Check Rundeck - CRITICAL {}".format(ex.message))
            sys.exit(self.ExitCode.CRITICAL.value)


if __name__ == '__main__':
    check = CheckRundeck()

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument('--host', default='http://127.0.0.1:4440',
                        help='Host of the Apache-MQ REST service')
    parser.add_argument('-t', '--token', default='12345', help='Token for authentication')

    subparsers = parser.add_subparsers(dest='command')

    queueu_parser = subparsers.add_parser('health')

    args = parser.parse_args()

    check.token = args.token
    check.url = args.host

    if args.command == 'health':
        check.get_health_status()
    else:
        print("invalid call! not enough parameters")
        sys.exit(1)
